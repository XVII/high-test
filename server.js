require('isomorphic-fetch')
const express = require('express')
const app = express()
const port = 3000
const ejs = require('ejs')
const fs = require('fs')

const offersCache = {};
let counter = 0;

app.get('/:token', async (req, res) => {
  console.time(`[${counter}] GET`);
  const token = req.params.token;
  const offers = await getOffers(token);
  const path = await compileHtml(token, counter);
  res.sendFile(path);
  console.timeEnd(`[${counter}] GET`);
  counter++;
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})

function getOffers(token) {
  return new Promise((resolve, reject) => {
    if (offersCache[token]) {
      return resolve(offersCache[token]);
    }

    fetch(`https://api.cityads.com/api/rest/webmaster/json/offers/api?remote_auth=${token}`)
      .then(res => res)
      .then(res => res.json())
      .then(data => {
        offersCache[token] = Object.values(data?.data?.items ?? {});
        resolve(offersCache[token]);
      }).catch(err => {
        reject(err);
      })

  })
}

function compileHtml(token, counter) {
  const path = `/app/static/compiled/${token}.html`;
  return new Promise((resolve, reject) => {

    fs.stat(path, function(err, stats){
      if (err) {
        const data = offersCache[token];
        console.time(`[${counter}] Create cached html`);
        ejs.renderFile('/app/static/index.ejs', {
          offers: data
        }, {}, function(err, html) {
          if (err) {
            return reject(err);
          }

          fs.writeFile(path, html, function(){
            console.time(`[${counter}] Create cached html`);
            resolve(path);
          });

        });
      }
      else {
        return resolve(path)
      }
    })
  })
}